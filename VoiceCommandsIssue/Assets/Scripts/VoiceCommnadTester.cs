﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WearHFPlugin;

public class VoiceCommnadTester : MonoBehaviour
{
    [SerializeField]
    WearHF rw;
    [SerializeField]
    Text text;

    void Start()
    {
        rw.AddVoiceCommand("test", voiceCommand => { text.text = "Time: " + Time.time.ToString(); });
        rw.AddVoiceCommand("late", voiceCommand => { text.text = "late command is working"; });
    }

    void Update()
    {
        
    }
}
